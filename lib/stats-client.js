/* jshint globalstrict:true, trailing:false, unused:true, node:true */

'use strict';

const processName = require('./process-name');

function emergencyLog() {
  console.error.apply(console, arguments);
}

exports.create = function (options) {
  const { StatsD } = require('node-statsd');
  const statsdNodeJsVersionTags = require('./statsd-nodejs-version-tags');
  const path = require('path');

  // Stats requires a logger and logger requires stats, so
  // we'll forego logging in the stats module rather than the
  // other way around
  const { config } = options;
  const { tags } = options;
  const { prefix } = options;

  const statsdEnabled = config.getBool('stats:statsd:enabled');

  let statsdClient;
  /**
   * statsd
   */
  if (statsdEnabled) {
    let globalTags = [];

    if (config.get('staging')) {
      globalTags.push('staging:1');
    } else {
      globalTags.push('staging:0');
    }

    if (tags) globalTags = globalTags.concat(tags);

    if (options.includeNodeVersionTags) {
      globalTags = globalTags.concat(statsdNodeJsVersionTags());
    }

    const appName = processName.getShortProcessName();
    if (appName) {
      globalTags.push(`job:${appName}`);
    }

    // Add the main entry point into the app as a tag
    if (require.main && require.main.filename) {
      const { filename } = require.main;
      const entry = path.basename(filename, '.js');
      globalTags.push(`main:${entry}`);
    }

    statsdClient = new StatsD({
      prefix,
      host: '127.0.0.1',
      global_tags: globalTags,
    });

    statsdClient.socket.on('error', (error) =>
      emergencyLog(`Error in statsd socket: ${error}`, { exception: error })
    );
  } else {
    // Just create a mock client
    statsdClient = new StatsD({ mock: true });
  }

  return statsdClient;
};
