'use strict';

function configureNodeEnv() {
  const nodeEnv = process.env.NODE_ENV;
  if (nodeEnv) return nodeEnv;

  /* Default to NODE_ENV=dev */
  process.env.NODE_ENV = 'dev';
  return 'dev';
}

/* Factory */
exports.create = function (configDirectory) {
  const nconf = require('nconf');
  const path = require('path');
  const { EventEmitter } = require('events');
  const Promise = require('bluebird');

  const SYSTEM_CONFIG_DIR = '/etc/gitter/';

  /* Load configuration parameters */
  const nodeEnv = configureNodeEnv();

  let staging = process.env.STAGING;
  staging = staging === '1' || staging === 'true';

  let configDir = configDirectory;
  if (process.env.GITTER_CONFIG_DIR_OVERRIDE) {
    configDir = process.env.GITTER_CONFIG_DIR_OVERRIDE;
    console.log(`Overridden config directory: ${configDir}`);
  }

  // Monkey-patch an event-emitter onto nconf (for now)
  nconf.events = new EventEmitter();

  nconf.argv().env('__');

  nconf.add('envUser', {
    type: 'file',
    file: path.join(configDir, `config.${nodeEnv}.user-overrides.json`),
  });

  nconf.add('user', {
    type: 'file',
    file: path.join(configDir, 'config.user-overrides.json'),
  });

  if (staging) {
    nconf.add('systemStagingOverrides', {
      type: 'file',
      file: path.join(SYSTEM_CONFIG_DIR, `config.${nodeEnv}-staging.overrides.json`),
    });
  }

  nconf.add('systemOverrides', {
    type: 'file',
    file: path.join(SYSTEM_CONFIG_DIR, `config.${nodeEnv}.overrides.json`),
  });

  if (staging) {
    nconf.add('nodeEnvStaging', {
      type: 'file',
      file: path.join(configDir, `config.${nodeEnv}-staging.json`),
    });
  }

  nconf.add('nodeEnv', {
    type: 'file',
    file: path.join(configDir, `config.${nodeEnv}.json`),
  });
  nconf.add('systemDefaults', {
    type: 'file',
    file: path.join(SYSTEM_CONFIG_DIR, `config.${nodeEnv}.json`),
  });

  nconf.add('defaults', {
    type: 'file',
    file: path.join(configDir, 'config.default.json'),
  });

  process.on('SIGHUP', () =>
    Promise.map([nconf.stores.user, nconf.stores.nodeEnv, nconf.stores.defaults], (store) =>
      Promise.fromCallback((callback) => store.load(callback))
    )
      .then(() => {
        nconf.events.emit('reload');
        return null;
      })
      .catch((err) => {
        console.log(`gitter-config: Reload failed: ${err}`, err);
      })
  );

  nconf.runtimeEnvironment = nodeEnv;

  function parseBool(value) {
    switch (typeof value) {
      case 'boolean':
        return value;
      case 'number':
        return Boolean(value);
      case 'string':
        return Boolean(JSON.parse(value));
      default:
        return undefined;
    }
  }

  /**
   * Monkey-patch `getBool`
   */
  nconf.getBool = function (key) {
    const value = this.get(key);
    return parseBool(value);
  };

  return nconf;
};
