'use strict';

const _ = require('lodash');

function emergencyLog() {
  console.error.apply(console, arguments);
}

function getBucketFromUserId(userId) {
  const lastChar = userId.toString().slice(-1);
  return parseInt(String(lastChar), 16) % 2 ? 'A' : 'B';
}

function configureLogger(statsHandlers, config, logger) {
  statsHandlers.event.push((eventName, properties) => {
    if (!properties) return;

    // Strip out dodgy named attributes which may screw with Kibana
    const meta = _.omit(
      properties,
      'agent:type',
      'agent:family',
      'agent:version',
      'agent:device:family',
      'agent:os:family',
      'agent:os:version'
    );

    meta.eventName = eventName;

    logger.info('stats.event', meta);
  });
}

function configureCube(statsHandlers, config) {
  const Cube = require('@gitterhq/cube');
  const statsUrl = config.get('stats:cube:cubeUrl');
  const cube = Cube.emitter(statsUrl);

  statsHandlers.event.push((eventName, properties) => {
    if (!properties) properties = {};

    properties.env = config.runtimeEnvironment;

    const event = {
      type: `gitter_${eventName.replace(/\./g, '_')}`,
      time: new Date(),
      data: properties,
    };
    cube.send(event);
  });

  statsHandlers.charge.push((userId, usd) => {
    const event = {
      type: 'gitter_charge',
      time: new Date(),
      data: {
        userId,
        usd,
        env: config.runtimeEnvironment,
      },
    };
    cube.send(event);
  });
}

function configureStatsD(statsHandlers, config) {
  const statsdClient = require('./stats-client').create({
    config,
    prefix: config.get('stats:statsd:prefix'),
  });

  statsHandlers.event.push((eventName) => {
    statsdClient.increment(eventName);
  });

  statsHandlers.charge.push((userId, usd) => {
    statsdClient.increment('charged', usd);
  });

  statsHandlers.eventHF.push((eventName, count, frequency, tags) => {
    if (!count) count = 1;
    if (!frequency) frequency = 0.1;

    /* Only send to the server one tenth of the time */
    statsdClient.increment(eventName, count, frequency, tags);
  });

  statsHandlers.gaugeHF.push((gaugeName, value, frequency, tags) => {
    if (!frequency) frequency = 0.1;

    /* Only send to the server one tenth of the time */
    statsdClient.gauge(gaugeName, value, frequency, tags);
  });

  statsHandlers.responseTime.push((timerName, duration, tags) => {
    statsdClient.timing(timerName, duration, 1, tags);
  });
}

function configureIntercom(statsHandlers, config) {
  const intercomWhiteList = {
    new_chat: true,
    user_login: true,
    join_room: true,
    create_room: true,
    create_group: true,
    new_group: true,
    join_group: true,
    leave_group: true,
    user_added_someone: true,
    new_invited_user: true,
    invite_accepted: true,
    new_user: true,
    charge_failed: true,
    lurk_room: true,
    gitter_supporter: true,
    gitlab_ml_opt_in: true,
  };

  const Intercom = require('intercom.io');
  const intercomOptions = {
    apiKey: config.get('stats:intercom:key'),
    appId: config.get('stats:intercom:app_id'),
  };

  const intercom = new Intercom(intercomOptions);

  statsHandlers.event.push((eventName, properties) => {
    /**
     * Metadata can be used to submit an event with extra key value data.
     * Each event can contain up to five metadata key values.
     * https://doc.intercom.io/api#event-model
     */
    function selectIntercomMetaData(properties) {
      const result = {};
      const keys = Object.keys(properties);
      const totalKeys = 0;
      for (let i = 0; i < keys.length && totalKeys <= 5; i++) {
        const key = keys[i];
        if (key === 'userId') continue; // Already have this in the event

        const value = properties[key];
        const valueType = typeof value;
        if (valueType === 'string' || valueType === 'number' || valueType === 'boolean') {
          result[key] = value;
        }
      }
      return result;
    }

    if (!intercomWhiteList[eventName]) return;
    // intercom likes a seconds since epoch date not ms that node gives by default
    const now = Number(Date.now() / 1000).toFixed(0);
    let attemptCount = 0;
    const eventData = {
      event_name: eventName,
      created_at: now,
      user_id: properties.userId,
    };

    if (properties) {
      eventData.metadata = selectIntercomMetaData(properties);
    }

    function createEvent() {
      intercom.createEvent(eventData, (err, res) => {
        if (err || (res.errors && res.errors.length)) {
          attemptCount++;
          if (attemptCount < 2) {
            setTimeout(createEvent, 2000);
          } else {
            emergencyLog(`Intercom error: ${err}`, { exception: err });
          }
        }
      });
    }
    createEvent();
  });

  statsHandlers.charge.push((userId, usd) => {
    // intercom likes a seconds since epoch date not ms that node gives by default
    const now = Number(Date.now() / 1000).toFixed(0);
    let attemptCount = 0;
    function createEvent() {
      intercom.createEvent(
        {
          event_name: 'charged',
          created_at: now,
          user_id: userId,
          metadata: {
            value: usd,
          },
        },
        (err, res) => {
          if (err || (res.errors && res.errors.length)) {
            attemptCount++;
            if (attemptCount < 2) {
              setTimeout(createEvent, 2000);
            } else {
              emergencyLog(`Intercom error: ${err}`, { exception: err });
            }
          }
        }
      );
    }
    createEvent();
  });

  statsHandlers.userUpdate.push((user /* , properties */) => {
    const created_at = new Date(user._id.getTimestamp());

    const profile = {
      email: user.email,
      user_id: user._id,
      name: user.displayName,
      created_at,
      // "last_impression_at": now,
      update_last_request_at: true,
      custom_data: {
        username: user.username,
        bucket: getBucketFromUserId(user._id),
        state: user.state || 'ACTIVE',
      },
    };

    intercom.createUser(profile, (err) => {
      if (err) {
        emergencyLog(`Intercom error: ${err}`, { exception: err });
      }
    });
  });
}

function configureGA(statsHandlers, config) {
  const ua = require('universal-analytics');
  const gaID = config.get('stats:ga:key');
  if (!gaID) return emergencyLog(new Error('Missing key for Google Analytics'));

  statsHandlers.event.push((eventName, properties) => {
    // Don't handle events that don't have a googleAnalyticsUniqueId
    if (!properties || !properties.googleAnalyticsUniqueId) return;
    const visitor = ua(gaID, properties.googleAnalyticsUniqueId);

    visitor.event(
      {
        ec: 'stats',
        ea: eventName,
        // Label: el: "",
        // Value: ev: 0,
        // Page: dp: "/"
      },
      (err) => {
        if (err)
          emergencyLog(`Google Analytics event error: ${err}`, {
            exception: err,
          });
      }
    );
  });

  statsHandlers.trackRequest.push((req) => {
    let UUID;

    // checking for existent UUID
    if (req.cookies && req.cookies._ga) {
      UUID = req.cookies._ga.split('.').slice(2, 4).join('.');
    }

    const visitor = ua(gaID, UUID);
    visitor.pageview(
      {
        dp: req.url,
        dh: req.get('host'),
        dr: req.get('referer'),
        ua: req.get('User-Agent'),
      },
      (err) => {
        if (err)
          emergencyLog(`Google Analytics Track Request: ${err}`, {
            exception: err,
          });
      }
    );
  });
}

exports.create = function (options) {
  // Stats requires a logger and logger requires stats, so
  // we'll forego logging in the stats module rather than the
  // other way around
  const { config } = options;
  const { logger } = options;

  const statsHandlers = {
    trackRequest: [],
    event: [],
    charge: [],
    eventHF: [],
    gaugeHF: [],
    userUpdate: [],
    responseTime: [],
    alias: [],
  };

  const gaEnabled = config.getBool('stats:ga:enabled');
  const statsdEnabled = config.getBool('stats:statsd:enabled');
  const cubeEnabled = config.getBool('stats:cube:enabled');
  const loggerEnabled = config.getBool('stats:logger:enabled');
  const intercomEnabled = config.getBool('stats:intercom:enabled');

  /**
   * console
   */
  if (loggerEnabled) {
    configureLogger(statsHandlers, config, logger);
  }

  /**
   * cube
   */
  if (cubeEnabled) {
    configureCube(statsHandlers, config);
  }

  /**
   * statsd
   */
  if (statsdEnabled) {
    configureStatsD(statsHandlers, config);
  }

  /**
   * Intercom
   */
  if (intercomEnabled) {
    configureIntercom(statsHandlers, config);
  }

  /**
   * Request Tracking for Google Analytics
   */
  if (gaEnabled) {
    configureGA(statsHandlers, config);
  }

  function makeHandler(handlers) {
    if (!handlers.length) return function () {};

    if (handlers.length === 1) {
      /**
       * Might seem a bit over the top, but these handlers get
       * called a lot, so we should make this code perform if we can
       */
      const handler = handlers[0];
      return function () {
        const args = arguments;

        try {
          handler.apply(null, args);
        } catch (err) {
          emergencyLog(`[stats] Error processing event: ${err}`, {
            exception: err,
          });
        }
      };
    }

    return function () {
      const args = arguments;

      handlers.forEach((handler) => {
        try {
          handler.apply(null, args);
        } catch (err) {
          emergencyLog(`[stats] Error processing event: ${err}`, {
            exception: err,
          });
        }
      });
    };
  }

  return Object.keys(statsHandlers).reduce((memo, method) => {
    memo[method] = makeHandler(statsHandlers[method]);
    return memo;
  }, {});
};
