'use strict';

function create(options) {
  const { config } = options;

  if (config.get('amazonses:accessKeyId')) {
    // eslint-disable-next-line global-require
    return require('./mailers/amazon-ses').create(options);
  }

  if (config.get('mandrill:apiKey')) {
    // eslint-disable-next-line global-require
    return require('./mailers/mandrill').create(options);
  }

  // eslint-disable-next-line global-require
  return require('./mailers/fake-mailer').create(options);
}

module.exports = {
  create,
};
