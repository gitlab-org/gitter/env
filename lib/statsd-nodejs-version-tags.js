'use strict';

module.exports = function () {
  const tags = [];
  const nodeFullVersion = process.versions.node;
  if (nodeFullVersion) {
    tags.push(`node_ver:${nodeFullVersion}`);
    const nodeMinorVersion = nodeFullVersion.replace(/^(\d+)\.(\d+)\..*/, '$1.$2');

    if (nodeMinorVersion) {
      tags.push(`node_minor_ver:${nodeMinorVersion}`);
    }
  }

  return tags;
};
