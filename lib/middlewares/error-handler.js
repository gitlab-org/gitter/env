'use strict';

const _ = require('lodash');

module.exports = function errorHandlerMiddleware(err, req, res /*, next*/) {
  // eslint-disable-line no-unused-vars
  const message = res.locals.errorMessage || 'Internal Server Error';

  // TODO: Add an HTML version for HTML clients
  if (res.headersSent) {
    // We escape `message` to avoid it getting injected into the middle of a HTML response (XSS)
    res.write(
      JSON.stringify({
        error: _.escape(message),
        uniqueId: res.locals.errorIdentifer,
      })
    );
    res.end();
  } else {
    res.send({ error: message, uniqueId: res.locals.errorIdentifer });
  }
};
