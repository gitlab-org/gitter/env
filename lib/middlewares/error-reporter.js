/* jshint globalstrict: true, trailing: false, unused: true, node: true */

'use strict';

const debug = require('debug')('gitter:http-error');
const http = require('http');

/**
 * Returns an integer value is possible
 * or undefined otherwise
 */
function asStatusCode(value) {
  if (!value) return;

  if (typeof value === 'number') return value;

  if (typeof value === 'string') {
    const v = parseInt(value, 10);
    if (v && v >= 400 && v < 600) return v;
  }
}

/**
 * Given an error, return a status code and message and the message to send to
 * the client
 */
function getStatusAndMessage(err) {
  const errorIsStatusCode = asStatusCode(err);
  let message;

  // This deals with the deprecated use-case
  // of `throw 500` or `callback(500)`
  if (errorIsStatusCode) {
    message = http.STATUS_CODES[errorIsStatusCode] || http.STATUS_CODES[500];
    return {
      status: errorIsStatusCode,
      message,
      clientMessage: message,
    };
  }

  const status = asStatusCode(err.status || err.statusCode) || 500;
  message = err.message || http.STATUS_CODES[status] || http.STATUS_CODES[500];
  const clientMessage = err.clientMessage || http.STATUS_CODES[status] || http.STATUS_CODES[500];

  return { status, message, clientMessage };
}

exports.create = function (options) {
  const { logger } = options;
  const { statsClient } = options;
  const { errorReporter } = options;

  function stat(name, req, additionalTags) {
    const { user } = req;
    const username = user && user.username;
    const tags = additionalTags || [];

    if (username) {
      tags.push(`user:${username}`);
    }

    if (req.path) {
      tags.push(`path:${req.path}`);
    }

    debug('stat: %s %j', name, tags);

    statsClient.increment(name, 1, 1, tags);
  }
  /* Has to have four args */
  return function (err, req, res, next) {
    const { user } = req;
    const userId = user && user.id;

    debug('Error: %s', err.stack);

    const statusAndMessage = getStatusAndMessage(err);
    const { status } = statusAndMessage;
    const { message } = statusAndMessage;
    const { clientMessage } = statusAndMessage;

    res.status(status);

    let errorIdentifer;
    switch (status) {
      case 401:
        stat('client_error_401', req);
        break;
      case 402:
        stat('client_error_402', req);
        break;
      case 403:
        stat('client_error_403', req);
        break;
      case 404:
        stat('client_error_404', req);
        break;
      case 429:
        stat('client_error_429', req);
        break;
      default:
        if (status >= 400 && status < 500) {
          stat('client_error_4xx', req, [`status:${status}`]);
        } else if (status >= 500) {
          // Use a unique-ish identifier so that
          // users can report errors
          errorIdentifer = Date.now();

          // Send to sentry
          errorReporter(
            err,
            {
              type: 'response',
              status,
              userId,
              username: user && user.username,
              url: req.url,
              method: req.method,
              route: req.routeIdentifier,
              clientMessage,
              errorIdentifer,
            },
            {
              // no tags
            },
            req
          );

          // Send to statsd
          stat('client_error_5xx', req, [`status:${status}`]);

          logger.error('An unexpected error occurred', {
            method: req.method,
            url: req.url,
            status,
            userId,
            username: user && user.username,
            message,
            clientMessage,
            errorIdentifer,
          });

          if (err.stack) {
            logger.error(`Error: ${err.stack}`);
          }
        }
    }

    res.locals.errorStatus = status;
    res.locals.errorMessage = clientMessage;
    res.locals.errorIdentifer = errorIdentifer;

    next(err);
  };
};
