'use strict';

const responseTime = require('response-time');
const debug = require('debug')('gitter:http');

function getPathTag(req) {
  // This is put here by the identify-route middleware
  if (req.routeIdentifier) {
    return req.routeIdentifier;
  }

  // Fallback to the old method...
  let path = req.route && req.route.path;
  if (!path) {
    return;
  }

  path = path
    .replace(/(\.:format\?|\([^)]*\))/g, '')
    .replace(/:/g, '_')
    .replace(/[^\w\-/]/g, '');

  return path;
}

exports.create = function (options) {
  const { config } = options;

  const statsdClient = require('../stats-client').create({
    config,
    includeNodeVersionTags: true,
  });

  return responseTime((req, res, time) => {
    const routeTag = getPathTag(req);

    const tags = [`method:${req.method}`, `sc:${res.statusCode}`];
    if (routeTag) {
      tags.push(`route:${routeTag}`);
    }

    debug(
      '%s %s [%s]<->[%s] completed %s with in %sms',
      req.method,
      req.originalUrl,
      req.routeIdentifier,
      routeTag,
      res.statusCode,
      time
    );

    statsdClient.histogram('http.request', time, tags);
  });
};
