/* jshint globalstrict:true, trailing:false, unused:true, node:true */

'use strict';

const processName = require('./process-name');

exports.create = function (options) {
  const raven = require('raven');
  const path = require('path');
  const fs = require('fs');
  const _ = require('lodash');
  const os = require('os');

  const { logger } = options;
  const nconf = options.config;

  const release = getRelease();

  function getTags(supplied) {
    try {
      const tags = _.extend(
        {},
        {
          host: os.hostname(),
          release,
        },
        supplied
      );

      const appName = processName.getShortProcessName();
      if (appName) {
        tags.job = appName;
      }
      return tags;
    } catch (e) {
      return supplied;
    }
  }

  function getExtras(err, supplied) {
    try {
      // Sentry doesn't actually do a toString on the error,
      // which can be very useful to diagnosing some types of errors
      return _.extend({}, { errorString: err && err.toString() }, supplied);
    } catch (e) {
      return supplied;
    }
  }

  // Add release information for sentry to help tracking when errors started
  function getRelease() {
    try {
      const assetTag = path.join(path.dirname(require.main.filename), 'ASSET_TAG');

      const stat = fs.statSync(assetTag);
      if (stat && stat.isFile()) {
        const release = fs.readFileSync(assetTag, { encoding: 'utf8' });
        return release;
      }
    } catch (e) {
      // no-op
    }
  }

  if (!nconf.get('errorReporting:enabled')) {
    return function (err, extra, tags) {
      logger.error('No error reporting is enabled so just logging to logger: ', {
        exception: err,
        meta: getExtras(err, extra),
        tags: getTags(tags),
      });
    };
  }

  const ravenUrl = nconf.get('errorReporting:ravenUrl');
  const client = new raven.Client(ravenUrl, { release });

  client.on('error', (err) => {
    logger.error('Unable to log error to sentry:', {
      exception: err,
    });
  });

  return function (err, extra, tags, request) {
    try {
      client.captureException(err, {
        // https://docs.sentry.io/clients/node/usage/#raven-node-additional-data
        extra: getExtras(err, extra),
        tags: getTags(tags),
        request,
      });
    } catch (err) {
      logger.error('Unable to log error to sentry:', {
        exception: err,
      });
    }
  };
};
