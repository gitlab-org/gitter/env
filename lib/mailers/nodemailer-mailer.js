'use strict';

const Promise = require('bluebird');
const path = require('path');
const HandlebarsMemoizer = require('./templates/handlebars-memoizer');

const LEGACY_MANDRILL_TEMPLATES = 'legacy_mandrill_templates';

class NodeMailerMailer {
  constructor(transporter, options) {
    this.transporter = transporter;
    this.logger = options.logger;
    this.stats = options.stats;
    this.config = options.config;

    this.defaultFromEmailName = options.defaultFromEmailName;
    this.defaultFromEmailAddress = options.defaultFromEmailAddress;

    this.overrideTo = options.overrideTo;

    this.handlebarsCache = new HandlebarsMemoizer();
  }

  async send(options) {
    try {
      const { templateName } = options;
      if (!templateName) throw new Error('templateName required');

      const data = await Promise.props(options.data || {});

      const [html, text] = await this.applyTemplates(templateName, data);
      const sendOptions = this.getSendOptions(options, html, text);

      const result = await this.transporter.sendMail(sendOptions);

      /* Email sent successfully */
      this.logger.verbose(`Sent email: ${templateName}: ${result.id}`);
      this.stats.event('email_sent');

      const { tracking } = options;
      if (tracking) {
        this.stats.event(tracking.event, tracking.data);
      }

      return {
        id: result && result.messageId,
        status: 'sent',
      };
    } catch (err) {
      /* Email sent failure */
      this.logger.error(`Email send failed: ${err}`, { exception: err });

      this.stats.event('email_send_failed');
      throw err;
    }
  }

  async applyTemplates(templateName, data) {
    /*
     * These will get replaced at a later stage with a more flexible scheme
     */
    const templateHtmlFilename = path.join(
      __dirname,
      LEGACY_MANDRILL_TEMPLATES,
      `${templateName}_html.hbs`
    );
    const templateTextFilename = path.join(
      __dirname,
      LEGACY_MANDRILL_TEMPLATES,
      `${templateName}_text.hbs`
    );

    return Promise.join(
      this.handlebarsCache.get(templateHtmlFilename),
      this.handlebarsCache.get(templateTextFilename),
      (htmlTemplate, textTemplate) => [htmlTemplate(data), textTemplate(data)]
    );
  }

  getSendOptions(options, html, text) {
    return {
      replyTo: options.replyTo || undefined,
      from: options.from || {
        name: options.fromName || this.defaultFromEmailName,
        address: options.fromEmail || this.defaultFromEmailAddress,
      },
      to: [this.overrideTo || options.to],
      bcc: options.bcc ? [options.bcc] : undefined,
      subject: options.subject,
      text,
      html,
    };
  }
}

module.exports = NodeMailerMailer;
