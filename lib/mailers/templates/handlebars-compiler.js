'use strict';

const handlebars = require('handlebars').create();
const fs = require('fs');
const Promise = require('bluebird');

function compiler(sourceFileName) {
  return Promise.fromCallback((callback) => {
    fs.readFile(sourceFileName, 'utf-8', callback);
  }).then((source) => handlebars.compile(source));
}

module.exports = compiler;
