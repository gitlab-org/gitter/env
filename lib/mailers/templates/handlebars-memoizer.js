'use strict';

const handlebarsCompiler = require('./handlebars-compiler');

function HandlebarsMemoizer() {
  this.compiled = {};
}

HandlebarsMemoizer.prototype = {
  get(sourceFileName) {
    if (this.compiled[sourceFileName]) {
      return this.compiled[sourceFileName];
    }

    const promise = handlebarsCompiler(sourceFileName);
    this.compiled[sourceFileName] = promise;
    return promise;
  },
};

module.exports = HandlebarsMemoizer;
