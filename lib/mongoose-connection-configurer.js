'use strict';

const mongodbConnString = require('mongodb-connection-string');
const mongodbArbiterDiscover = require('mongodb-arbiter-discovery');
const mongoDogStats = require('mongodb-datadog-stats');
const shutdown = require('shutdown');

module.exports = function mongooseConnectionConfigurer(options) {
  const { mongoose } = options;
  const { config } = options;
  const { logger } = options;
  const { env } = options;

  const { connection } = mongoose;

  /* Install dogstats */
  mongoDogStats.install(mongoose.mongo, {
    statsClient: env.createStatsClient({ includeNodeVersionTags: true }),
    sampleRate: 0.5,
  });

  const mongoConnection = config.get('mongo:connection');
  let mongooseConnectionUrl = config.get('mongo:url');
  // Sets the number of milliseconds a socket stays inactive before closing during the
  // connection phase of the driver
  const connectTimeoutMS = config.get('mongo:connectTimeoutMS') || 60000;
  // Sets the amount of time the driver waits for an inactive socket after the driver
  // has successfully connected before closing (0 will never timeout)
  const socketTimeoutMS = config.get('mongo:socketTimeoutMS') || 0;

  let autoIndex;
  if (config.get('mongo:noAutoIndex')) {
    autoIndex = false;
  } else {
    autoIndex = true;
  }

  const mongooseConnectOptions = {
    // New way to configure things with `useMongoClient`. We're able to use
    // `useMongoClient` because we have a Gitter forked version of `mongoose@4.6.8`.
    useMongoClient: true,
    keepAlive: 1,
    autoReconnect: true,
    connectTimeoutMS,
    socketTimeoutMS,

    // Old way to configure things. This is just in place for the downstream users of
    // `@gitterhq/env` that haven't updated to using the Gitter fork version of
    // `mongoose` yet.
    server: {
      keepAlive: 1,
      autoReconnect: true,
      socketOptions: { keepAlive: 1, connectTimeoutMS, socketTimeoutMS },
    },
    replset: {
      keepAlive: 1,
      autoReconnect: true,
      socketOptions: { keepAlive: 1, connectTimeoutMS, socketTimeoutMS },
    },

    // Other config
    config: {
      autoIndex,
    },
  };

  if (mongoConnection) {
    mongooseConnectionUrl = mongodbConnString.mongo(mongoConnection);
  }
  mongoose.connect(mongooseConnectionUrl, mongooseConnectOptions);

  shutdown.addHandler('mongo', 1, (callback) => {
    mongoose.disconnect(callback);
  });

  let hasConnected = false;

  mongoose.connection.once('open', () => {
    hasConnected = true;
  });

  let autodiscoveryAttempts = 0;

  function attemptAutoDiscoveryConnection(err) {
    logger.error(`Attempting autodiscovery as connection "${mongoConnection}" failed`);

    function die(err) {
      console.error(err);
      console.error(err.stack);
      shutdown.shutdownGracefully(1);
    }

    if (autodiscoveryAttempts > 0) {
      logger.error('No autodiscovery already failed. Unable to continue.');
      /* Shutdown */
      return die(err);
    }

    autodiscoveryAttempts++;

    const autoDiscoveryHost = config.get('mongo:autoDiscovery:host');
    const autoDiscoveryPort = config.get('mongo:autoDiscovery:port') || 27017;
    const replicaSet =
      mongoConnection && mongoConnection.options && mongoConnection.options.replicaSet;

    if (!autoDiscoveryHost) {
      /* Autodiscovery not configured */
      logger.error('No autodiscovery host available, unable to autodiscover');
      return die(err);
    }

    logger.info(`Autodiscovery via ${autoDiscoveryHost}:${autoDiscoveryPort}`);

    mongodbArbiterDiscover(
      {
        host: autoDiscoveryHost,
        port: autoDiscoveryPort,
        mongo: mongoose.mongo,
        replicaSet,
      },
      (err, hosts) => {
        if (err) return die(err);

        mongoConnection.hosts = hosts;
        mongoose.connect(mongodbConnString.mongo(mongoConnection), mongooseConnectOptions);
      }
    );
  }

  connection.once('error', (err) => {
    /* Only attempt auto-discovery if we haven't already connected */
    if (hasConnected) return;

    logger.error('MongoDB connection error', { exception: err });
    attemptAutoDiscoveryConnection(err);
  });
};
