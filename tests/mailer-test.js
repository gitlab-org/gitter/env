'use strict';

const Promise = require('bluebird');
const assert = require('assert');
const mockConfig = require('./mock-config');
const stats = require('../lib/stats');
const logger = require('../lib/logger');
const mailer = require('../lib/mailer');

const testMatrix = {
  // We no longer have access to the Mandrill API.
  // We switched from Mandrill to Amazon SES, see https://gitlab.com/gitterHQ/webapp/-/issues/2573
  //
  // mandrill: {
  //   config: {
  //     mandrill: {
  //       username: 'admin@troupe.co',
  //       apiKey: process.env.mandrill__apiKey,
  //     },
  //   },
  // },
  amazonSES: {
    config: {
      amazonses: {
        accessKeyId: process.env.amazonses__accessKeyId,
        secretAccessKey: process.env.amazonses__secretAccessKey,
        defaultFromEmailName: 'Gitter Notifications',
        defaultFromEmailAddress: 'no-reply@gitter.im',
      },
    },
  },
};

Object.keys(testMatrix).forEach((testMatrixKey) => {
  describe(`mailer (${testMatrixKey})`, () => {
    let mailerInstance;
    let config;
    let statsInstance;
    let loggerInstance;

    beforeEach(() => {
      config = mockConfig(testMatrix[testMatrixKey].config);

      // TODO We could do with mocks of these
      statsInstance = stats.create({ config });
      loggerInstance = logger.create({ config });

      mailerInstance = mailer.create({
        config,
        stats: statsInstance,
        logger: loggerInstance,
      });
    });

    it('should send emails', async () => {
      const result = await mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: 'Guten tag',
          NAME: 'Mister Test',
        },
      });

      assert(result.status);
      assert(result.id);
    });

    it('should send emails with bccs', async () => {
      const result = await mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        bcc: 'test2@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: 'Guten tag',
          NAME: 'Mister Test',
        },
      });

      assert(result.status);
      assert(result.id);
    });

    it('should send emails with unresolved promises', async () => {
      const result = await mailerInstance({
        subject: 'Test',
        fromName: 'Mailer test',
        to: 'test@gitter.im',
        templateName: 'test-template',
        data: {
          HELLO: Promise.delay(10).thenReturn('Guten morgen'),
          NAME: Promise.delay(10).thenReturn('Mister promise'),
        },
      });

      assert(result.status);
      assert(result.id);
    });
  });
});
