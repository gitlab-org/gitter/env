'use strict';

const ioredis = require('../lib/env-ioredis');
const mockConfig = require('./mock-config');
const redisSpec = require('./redis-spec');

describe('gitter-redis', () => {
  let logger;
  let factory;

  before(() => {
    logger = require('../lib/logger').create({ config: mockConfig({}) });
    factory = ioredis.create({ config: mockConfig({}), logger });
  });

  redisSpec(
    (options) => factory.createClient(options, logger),
    (client) => {
      factory.quitClient(client);
    }
  );
});
