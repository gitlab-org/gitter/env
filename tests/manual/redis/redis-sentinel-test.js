/* jshint globalstrict:true, trailing:false, unused:true, node:true */

'use strict';

process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err}`);
  process.exit(1);
});

const env = require('../../..').create(__dirname);

const client = env.redis.getClient();
const key = 'test:redis-sentinel-test';

let current = 0;

function error(err) {
  console.error('ERROR: ', err);
  setTimeout(increment, 5);
}

function increment() {
  console.log('>incr');
  client.incr(key, (err) => {
    if (err) return error(err);

    client.get(key, (err, value) => {
      if (err) return error(err);
      value = parseInt(value, 10);

      console.log('> value is ', value);
      if (value === current + 1) {
        current = value;
        setTimeout(increment, 5);
      } else {
        console.log('> value is ', value, ' but current is ', current);
        throw new Error('Inconsistent');
      }
    });
  });
}

client.set(key, 0, (err) => {
  if (err) throw err;
  increment();
});
