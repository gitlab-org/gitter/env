'use strict';

const assert = require('assert');
const transformMeta = require('../lib/transform-meta');

function CustomObject() {}

CustomObject.prototype.toString = function () {
  return 'custom';
};

describe('transform-meta', () => {
  it('should transform an empty hash', () => {
    assert.deepEqual(transformMeta({}), {});
  });

  it('should transform an simple hash', () => {
    assert.deepEqual(transformMeta({ hello: 'there' }), { hello: 'there' });
    assert.deepEqual(transformMeta({ hello: 1 }), { hello: 1 });
    assert.deepEqual(transformMeta({ hello: false }), { hello: false });
    assert.deepEqual(transformMeta({ hello: null }), { hello: null });
    assert.deepEqual(transformMeta({ hello: undefined }), {});
  });

  it('should transform an error', () => {
    const error = new Error('Some Error');
    assert.deepEqual(transformMeta({ error }), {
      error: { message: error.message, stack: error.stack, name: error.name },
    });
  });

  it('should transform an error string', () => {
    const errorString = 'lol-a-string-error';
    assert.deepEqual(transformMeta({ error: errorString }), {
      error: { message: errorString, stack: null, name: null },
    });
  });

  it('should transform an two-level deep hash', () => {
    assert.deepEqual(transformMeta({ hello: { there: 1 } }), {
      hello: { there: 1 },
    });
  });

  it('should ignore anything deeper than two levels', () => {
    assert.deepEqual(transformMeta({ hello: { there: { world: 1 } } }), {
      hello: {},
    });
    assert.deepEqual(
      transformMeta({
        hello: {
          there: {
            world: 1,
          },
        },
        world: {
          a: 1,
          b: {},
          c: new CustomObject(),
        },
      }),
      {
        hello: {},
        world: {
          a: 1,
          c: 'custom',
        },
      }
    );
  });
});
