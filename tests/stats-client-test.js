'use strict';

const assert = require('assert');
const mockConfig = require('./mock-config');
const statsClient = require('../lib/stats-client');

describe('gitter-stats-client', () => {
  it('should fire off all the events correctly', () => {
    process.env.JOB = 'test-1';

    const config = mockConfig({
      stats: {
        statsd: {
          enabled: true,
        },
      },
    });

    const client = statsClient.create({ config, tags: ['test:1'] });
    assert(client);
    assert(client.global_tags.indexOf('test:1') >= 0);
  });
});
